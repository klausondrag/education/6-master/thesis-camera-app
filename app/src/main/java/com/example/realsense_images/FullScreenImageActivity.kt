package com.example.realsense_images

import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_full_screen_image.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Math.abs
import java.net.URL

class FullScreenImageActivity : AppCompatActivity() {

    var baseUrl = ""
    var currentMode = "color"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image)

        if (intent != null
            && intent.getStringExtra("baseUrl") != null
            && intent.getStringExtra("mode") != null) {

            baseUrl = intent.getStringExtra("baseUrl")
            currentMode = "color"
            updateImage()

            fullScreenImageView.setOnTouchListener(object: OnSwipeTouchListener(this@FullScreenImageActivity) {
                override fun onSwipeLeft() {
                    super.onSwipeLeft()
                    if (currentMode == "color") {
                        currentMode = "depth"
                        updateImage()
                    }
                }

                override fun onSwipeRight() {
                    super.onSwipeRight()
                    if (currentMode == "depth") {
                        currentMode = "color"
                        updateImage()
                    }
                }
            })
        }
    }

    private fun updateImage() {
        CoroutineScope(Dispatchers.IO).launch {
            val urlObject = URL(baseUrl + currentMode)
            val bmp = BitmapFactory.decodeStream(urlObject.openConnection().getInputStream())
            withContext(Dispatchers.Main) {
                fullScreenImageView.setImageBitmap(bmp)
            }
        }
    }
}

// from https://stackoverflow.com/a/59560161
open class OnSwipeTouchListener(ctx: Context?) : View.OnTouchListener {
    private val gestureDetector: GestureDetector  = GestureDetector(ctx, GestureListener())

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        return gestureDetector.onTouchEvent(event)
    }

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        private val SWIPE_THRESHOLD = 100
        private val SWIPE_VELOCITY_THRESHOLD = 100

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            var result = false
            try {
                val diffY: Float = e2.y - e1.y
                val diffX: Float = e2.x - e1.x
                if (abs(diffX) > abs(diffY)) {
                    if (abs(diffX) > SWIPE_THRESHOLD && abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight()
                        } else {
                            onSwipeLeft()
                        }
                        result = true
                    }
                } else if (abs(diffY) > SWIPE_THRESHOLD && abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom()
                    } else {
                        onSwipeTop()
                    }
                    result = true
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
            return result
        }
    }

    open fun onSwipeRight() {}
    open fun onSwipeLeft() {}
    open fun onSwipeTop() {}
    open fun onSwipeBottom() {}
}