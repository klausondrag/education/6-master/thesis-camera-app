package com.example.realsense_images

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.preference.PreferenceManager
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.beust.klaxon.Klaxon
import com.example.realsense_images.R.layout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL


class MainActivity : AppCompatActivity() {

    var allIds = ArrayList<String>()
    lateinit var arrayAdapter: ArrayAdapter<Any?>
    lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)

        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        editTextUrl.setText(
            preferences.getString(
                "baseUrl",
                "http://192.168.12.249:8080"
            )
        )

        arrayAdapter = ArrayAdapter<Any?>(
            this@MainActivity,
            R.layout.row,
            R.id.textViewItem,
            allIds as List<Any?>
        )
        listView.adapter = arrayAdapter

        listView.setOnItemClickListener { parent, view, position, id ->
            val baseUrl = editTextUrl.text.toString()
            val imageId = allIds[position]
            openIntent(baseUrl, imageId)
        }

        buttonSync.setOnClickListener {
            textViewStatus.text = "Loading..."
            CoroutineScope(IO).launch {
                requestStatus(editTextUrl.text.toString())
            }
        }

        buttonTakePicture.setOnClickListener {
            textViewStatus.text = "Loading..."
            CoroutineScope(IO).launch {
                requestPicture(editTextUrl.text.toString())
            }
        }
    }

    private suspend fun requestStatus(baseUrl: String) {
        val urlString = "$baseUrl/health"

        val urlObject = URL(urlString)
        var viewText = ""
        val response: StatusResponse?
        try {
            val responseText = urlObject.readText()

            response = Klaxon().parse<StatusResponse>(responseText)
            viewText = response?.status.toString()

        } catch (e: Exception) {
            viewText = e.toString()
            return
        }
        withContext(Main) {
            textViewStatus.text = viewText
        }

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = preferences.edit()
        editor.putString("baseUrl", baseUrl)
        editor.apply()

        requestSync(baseUrl)
    }

    private suspend fun requestSync(baseUrl: String) {
        val urlString = "$baseUrl/images"

        val urlObject = URL(urlString)
        try {
            val responseText = urlObject.readText()
            val response = Klaxon().parseArray<String>(responseText)
            withContext(Main) {
                allIds.clear()
                response?.let { allIds.addAll(it) }
                arrayAdapter.notifyDataSetChanged()
            }
        } catch (e: Exception) {
            withContext(Main) {
                textViewStatus.text = e.toString()
            }
            return
        }
    }

    private suspend fun requestPicture(baseUrl: String) {
        val urlString = "$baseUrl/take-picture"

        val urlObject = URL(urlString)
        var viewText = ""
        val response: TakePictureResponse?
        try {
            val responseText = urlObject.readText()

            response = Klaxon().parse<TakePictureResponse>(responseText)
            viewText = (
                    "Status ${response?.status}"
                            + ", n tries: ${response?.n_tries}"
                            + "\nimage id: ${response?.image_id}"
                    )

        } catch (e: Exception) {
            viewText = e.toString()
            return
        }
        withContext(Main) {
            textViewStatus.text = viewText
        }
        response?.image_id?.let { openIntent(baseUrl, it) }
        withContext(Main) {
            allIds.add(0, response?.image_id.toString())
            arrayAdapter.notifyDataSetChanged()
        }
    }

    private fun openIntent(baseUrl: String, imageId: String) {
        val fullScreenIntent = Intent(this, FullScreenImageActivity::class.java);
        fullScreenIntent.putExtra("baseUrl", "$baseUrl/images/$imageId/")
        fullScreenIntent.putExtra("mode", "color")
        startActivity(fullScreenIntent)
    }

    data class StatusResponse(
        val status: String
    )

    data class TakePictureResponse(
        val status: String,
        val n_tries: Int,
        val image_id: String
    )

    data class ImagesResponse(
        val imageIds: ArrayList<String>
    )
}
